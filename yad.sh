#!/bin/bash

# Função para solicitar entrada do usuário com YAD
get_input() {
    result=$(yad --entry --title="SSH Connection" --text="Enter $1:" --width=300)
    
    # Verificar se o usuário cancelou a operação
    if [ $? -ne 0 ]; then
        response=$(yad --question --title="Confirmation" --text="Are you sure you want to cancel?")

        if [ $? -eq 0 ]; then
            yad --info --text="Operation canceled by user."
            exit 1
        else
            # O usuário optou por não cancelar, então reiniciamos a solicitação
            get_input "$1"
        fi
    fi

    echo "$result"
}

# Solicitar informações do usuário usando YAD
USER=$(get_input "your username")
HOST=$(get_input "the hostname or IP address")
PORT=$(yad --entry --title="SSH Connection" --text="Enter the SSH port (default is 22):" --entry-text="22" --numeric)

# Selecionar o arquivo para transferência
FILE=$(yad --file --title="Select File for Transfer" --width=500 --height=300)

# Verificar se o usuário cancelou a seleção de arquivo
if [ $? -ne 0 ]; then
    response=$(yad --question --title="Confirmation" --text="Are you sure you want to cancel?")
    
    if [ $? -eq 0 ]; then
        yad --info --text="Operation canceled by user."
        exit 1
    else
        FILE=$(yad --file --title="Select File for Transfer" --width=500 --height=300)
    fi
fi

# Exibir mensagem de confirmação
yad --info --text="Connecting to $USER@$HOST on port $PORT...\nTransferring file: $FILE"

# Executar scp em segundo plano
scp -P "$PORT" "$FILE" "$USER@$HOST:/remote/directory/" &

# Exibir mensagem de conclusão
yad --info --text="File transfer initiated successfully!"

exit 0
